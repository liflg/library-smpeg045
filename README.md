Website
=======
http://icculus.org/smpeg/

License
=======
LGPL license (see the file source/COPYING)

Version
=======
svn revision 399

Source
======
svn://svn.icculus.org/smpeg/tags/release_0_4_5/@399

Requires
========
* SDL1