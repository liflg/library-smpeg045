#!/bin/bash

set -e

select_multiarchname()
{
    echo "Please select the desired architecture:"
    echo "1): i386-linux-gnu"
    echo "2): x86_64-linux-gnu"
    echo "3): armeabi-linux-android"
    echo "4): armeabi_v7a-linux-android"
    read SELECTEDOPTION
    if [ x$SELECTEDOPTION = x"1" ]; then
        MULTIARCHNAME=i386-linux-gnu
    elif [ x$SELECTEDOPTION = x"2" ]; then
        MULTIARCHNAME=x86_64-linux-gnu
    elif [ x$SELECTEDOPTION = x"3" ]; then
        MULTIARCHNAME=armeabi-linux-android
    elif [ x$SELECTEDOPTION = x"4" ]; then
        MULTIARCHNAME=armeabi_v7a-linux-android
    else
        echo 'Invalid option selected!'
        select_multiarchname
    fi
}

linux_build()
{
    if [ x"$BUILDTYPE" = xdebug ]; then
        export CFLAGS="-ggdb"
        export CXXFLAGS="-ggdb"
    fi

    if [[ ( -z "$OPTIMIZATION" ) || ( "$OPTIMIZATION" -eq 2 ) ]]; then
        export CFLAGS="$CFLAGS -O2"
    else
        echo 'Optimization level '$OPTIMIZATION' is not yet implemented!'
        exit 1
    fi

    mkdir "$BUILDDIR"

    (cd source
    ./autogen.sh)

    ( SDLDIR="${PWD}/../library-sdl1/${MULTIARCHNAME}"
      export SDL_CFLAGS="-I${SDLDIR}/include/SDL -D_REENTRANT"
      export SDL_LIBS="-L${SDLDIR}/lib -lSDL -lpthread"
      cd "$BUILDDIR"
      ../source/configure \
         --prefix="$PREFIXDIR" \
         --disable-static \
         --disable-sdltest \
         --disable-rpath
      make -j`nproc` install
      rm -rf "$PREFIXDIR"/{bin,lib/*.la,share})

    # clean up afterwards
    ( cd source
      git clean -df .
      git checkout .)
}

if [ -z "$MULTIARCHNAME" ]; then
    echo '$MULTIARCHNAME is not set!'
    select_multiarchname
fi

if [ -z "$BUILDDIR" ]; then
    BUILDDIR="build_$MULTIARCHNAME"
fi

if [ -z "$PREFIXDIR" ]; then
    PREFIXDIR="$PWD/$MULTIARCHNAME"
fi

case "$MULTIARCHNAME" in
i386-linux-gnu)
    linux_build;;
x86_64-linux-gnu)
    linux_build;;
*)
    echo "$MULTIARCHNAME is not (yet) supported by this script."
    exit 1;;
esac

install -m664 source/COPYING "$PREFIXDIR"/lib/LICENSE-smpeg045.txt

rm -rf "$BUILDDIR"

echo "smpeg for $MULTIARCHNAME is ready."
